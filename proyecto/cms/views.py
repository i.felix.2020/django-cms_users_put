from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect

from .models import Contenido, Comentario

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
    elif request.method == "POST": # si tenemos un POST tenemos que comprobar la acción para ver que tipo de formulario a devolver
        action = request.POST['action']
        if action == "Enviar Contenido": # dentro de POST tenemos que detectar si estamos añadiendo un comentario o contenido
            valor = request.POST['valor']
    if request.method == "PUT" or (request.method == "POST" and action == "Enviar Contenido"):
        try:
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
            c.save()
    if request.method == "POST" and action == "Enviar Comentario":
            c = Contenido.objects.get(clave=llave)
            titulo = request.POST['titulo']
            cuerpo = request.POST['cuerpo']
            fecha = timezone.now()
            q = Comentario(contenido=c, titulo=titulo, cuerpo=cuerpo, fecha=fecha)
            q.save()
    try:
        contenido = Contenido.objects.get(clave=llave)
        context = {'contenido': contenido}
    except Contenido.DoesNotExist:
        context = {}
        return render(request, 'cms/not_content.html', context)
        # en el formulario not_content examino si el usuario está autenticado o no, si no lo está, no puede añadir contenido.

    return render(request, 'cms/content.html', context)

def loggedIn(request): # comprueba que haya una sesión abierta, en tal caso cuando pidamos loggedIn nos devolverá un usuario
    if request.user.is_authenticated: # comprobamos si el usuario está autenticado
        logged = "Logged in as " + request.user.username
    else: # en casod e que no, proporcionamos un link para poder logguearnos.
        logged = "Not logged in. <a href='/admin/'> Login here </a>"
    return HttpResponse(logged)

def logout_view(request):
    logout(request)  # con logout salimos de la sesión
    return redirect("/cms/")  # retornamos a la pagina principal de nuestra aplicación

def index(request):
    content_list = Contenido.objects.all()[:5]
    context = {'content_list': content_list}
    return render(request, 'cms/index.html', context)
